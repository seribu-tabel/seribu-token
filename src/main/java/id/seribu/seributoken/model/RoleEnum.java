package id.seribu.seributoken.model;

public enum RoleEnum{
    ROLE_SUPER,
    ROLE_ADMIN,
    ROLE_USER,
}