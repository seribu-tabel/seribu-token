package id.seribu.seributoken.constant;

public class SecurityConstants {
    
    public static final String SIGNING_KEY = "s3ribut4b3l";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String INFO ="/actuator/**";
    public static final String ROLES = "roles";
}