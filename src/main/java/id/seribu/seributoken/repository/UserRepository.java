package id.seribu.seributoken.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.seribu.seributoken.model.TUser;


@Repository
public interface UserRepository extends CrudRepository<TUser, UUID>{

    @Query(value = "select * from m_user where enabled = 'true' and username = :username", nativeQuery = true)
    TUser findByUsername(@Param("username") String username);
}