package id.seribu.seributoken.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.user.UserLoginDto;
import id.seribu.seributoken.constant.SecurityConstants;
import id.seribu.seributoken.model.TUser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @NonNull
    private final AuthenticationManager authenticationManager;
    @NonNull
    private final Long expirationTime;
    @NonNull
    private final String secret;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
        throws AuthenticationException {
        UserLoginDto loginDto = new UserLoginDto();
        try {
            loginDto = new ObjectMapper().readValue(request.getInputStream(), UserLoginDto.class);
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword(), new ArrayList<>()));
        } catch (IOException | AuthenticationException e) {
            log.error("User tidak memiliki akses: {}, pass: {}", loginDto.getUsername(), loginDto.getPassword(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain, Authentication authentication) {
        try {
            String jti = UUID.randomUUID().toString();
            TUser user = ((TUser) authentication.getPrincipal());
            String username = user.getUsername();

            String token = Jwts.builder().setId(jti).setSubject(username)
                .addClaims(this.getClaimAuthorities(user))
                .setExpiration(new Date(System.currentTimeMillis() + this.getExpirationTime()))
                .signWith(SignatureAlgorithm.HS512, secret.getBytes()).compact();
            response.addHeader("Access-Control-Expose-Headers", SecurityConstants.HEADER_STRING);
            response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write(new ObjectMapper().writeValueAsString(generateApiResponse(response.getStatus(), user)));
            response.getWriter().flush();
            log.info("{}.successfulAuthentication", this.getClass().getSimpleName());
        } catch (IOException e) {
            log.error("Error parsing object to json", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException failed)
			throws IOException, ServletException {
        UserLoginDto loginDto = new UserLoginDto();
        try{
            loginDto = new ObjectMapper().readValue(request.getInputStream(), UserLoginDto.class);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write(new ObjectMapper().writeValueAsString(generateApiResponse(response.getStatus(), loginDto)));
            response.getWriter().flush();
            log.info("{}.unsuccessfulAuthentication", this.getClass().getSimpleName());
        } catch (IOException e){
            log.error("Error parsing object to json", e);
            throw new RuntimeException(e);
        }
    }

    private Map<String, Object> getClaimAuthorities(TUser tUser){
        Map<String,Object> authorities = new HashMap<>();
        Set<String> data = new HashSet<>();
        Collection<? extends GrantedAuthority> value = tUser.getAuthorities();
        for(GrantedAuthority ga : value){
            data.add(ga.getAuthority());
        }
        authorities.put(SecurityConstants.ROLES, data);
        return authorities;
    }

    private Long getExpirationTime(){
        return expirationTime;
    }

    private <T> ApiResponse<T> generateApiResponse(final int status, T userData) {
        String message = "";
        if(status == 200){
            message = "Login berhasil";
            return new ApiResponse<>(status, message, userData);
        }else{
            message = "Silakan cek kembali username dan password Anda";
            return new ApiResponse<>(status, message, userData);
        }
    }
}





// import java.io.IOException;
// import java.util.Arrays;

// import javax.servlet.FilterChain;
// import javax.servlet.ServletException;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.core.authority.SimpleGrantedAuthority;
// import org.springframework.security.core.context.SecurityContextHolder;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UserDetailsService;
// import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
// import org.springframework.web.filter.OncePerRequestFilter;

// import id.seribu.constant.SecurityConstant;
// import io.jsonwebtoken.ExpiredJwtException;
// import io.jsonwebtoken.SignatureException;

// public class JwtAuthenticationFilter extends OncePerRequestFilter {

//     @Autowired
//     private UserDetailsService userDetailsService;

//     @Autowired
//     private JwtTokenUtil jwtTokenUtil;

//     @Override
//     protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
//         String header = req.getHeader(SecurityConstant.HEADER_STRING);
//         String username = null;
//         String authToken = null;
//         if (header != null && header.startsWith(SecurityConstant.TOKEN_PREFIX)) {
//             authToken = header.replace(SecurityConstant.TOKEN_PREFIX,"");
//             try {
//                 username = jwtTokenUtil.getUsernameFromToken(authToken);
//             } catch (IllegalArgumentException e) {
//                 logger.error("an error occured during getting username from token", e);
//             } catch (ExpiredJwtException e) {
//                 logger.warn("the token is expired and not valid anymore", e);
//             } catch(SignatureException e){
//                 logger.error("Authentication Failed. Username or Password not valid.");
//             }
//         } else {
//             logger.warn("couldn't find bearer string, will ignore the header");
//         }
//         if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

//             UserDetails userDetails = userDetailsService.loadUserByUsername(username);

//             if (jwtTokenUtil.validateToken(authToken, userDetails)) {
//                 UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
//                 authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
//                 logger.info("authenticated user " + username + ", setting security context");
//                 SecurityContextHolder.getContext().setAuthentication(authentication);
//             }
//         }

//         chain.doFilter(req, res);
//     }
// }